FROM golang:1.13.1

WORKDIR /app

COPY ./ .

EXPOSE 8080

CMD ["./main"]
